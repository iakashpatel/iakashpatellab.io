### PHP

Basic:

  - Syntax, Variables, Constants, data types, string, loops, conditions, operators, arrays
  - File handling, sessions, cookie
  - Functions, File Inclusion, error handling, http methods

Advance:

  - [[ https://www.tutorialspoint.com/php/php_regular_expression.htm | Php Regular Expressions ]]
  - Object oriented programming in php

Useful Resources:

[[ https://www.tutorialrepublic.com/php-tutorial/ | Basic in Php ]]
[[ https://www.onlinebuff.com/article_understand-object-oriented-programming-oops-concepts-in-php_46.html | Object oriented programming in Php ]]


### MySql Database


  - Schema Design
  - Query building (insert, update, delete, select, joins, aggregate functions)

Useful Resources:


[[ https://www.tutorialrepublic.com/php-tutorial/php-mysql-connect.php | Php connect to MySql ]]
[[ http://www.mysqltutorial.org/basic-mysql-tutorial.aspx | Getting started with MySQL ]]
[[ http://www.mysqltutorial.org/mysql-functions.aspx | MySql Funcitons ]]
[[ http://www.mysqltutorial.org/mysql-stored-procedure-tutorial.aspx | MySql stored procedure ]]
[[ https://www.sqlstyle.guide | SQL Style Guide ]]

### Laravel framework

Basic:

  - [[ https://getcomposer.org/doc/00-intro.md | Introduction to Composer ]]
  - Setup Laravel project, Add, remove packages using composer
  - Introduction to Laravel framework, request life cycle, routing, requests, responses, views (blade template), validations, middleware

Deeper dive in Laravel:

  - Event & Listeners, Jobs, Queues, Helpers, File storage
  - Create custom artisan commands, php unit testing (testcases, factories)
  - Seeders

Useful Resources:


[[ https://laravel.com/docs | Introduction to Laravel ]]

Series

[[ https://laracasts.com/series/laravel-from-scratch-2017 | Laravel ]]


### RestFul apis

- Create restful apis and API Resources in Laravel
- [[ https://laravel.com/docs/5.7/testing | Unit testing in Laravel ]]

Useful resource:

[[ https://phpunit.readthedocs.io/en/7.4/index.html | Php Unit testing ]]