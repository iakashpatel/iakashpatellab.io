* Install sublime package control

## User Settings

```
{
  "bold_folder_labels": true,
  "draw_minimap_border": true,
  "ensure_newline_at_eof_on_save": true,
  "fade_fold_buttons": false,
  "folder_exclude_patterns":
  [
    ".svn",
    ".git",
    "__pycache__",
    "node_modules",
    "*.min.js",
    "*.map",
    "*.pyc",
    "*.pyo",
    "*.exe",
    "*.dll",
    "*.obj",
    "*.o",
    "*.a",
    "*.lib",
    "*.so",
    "*.dylib",
    "*.ncb",
    "*.sdf",
    "*.suo",
    "*.pdb",
    "*.idb",
    ".DS_Store",
    "*.psd"
  ],
  "font_size": 15.0,
  "ignored_packages":
  [
    "-Phpcs"
  ],
  "line_padding_bottom": 1,
  "line_padding_top": 1,
  "rulers":
  [
    80
  ],
  "tab_size": 2,
  "translate_tabs_to_spaces": true,
  "trim_trailing_white_space_on_save": true,
  "vintage_start_in_command_mode": true,
  "word_separators": "./\\()\"'-:$,.;<>~!@#%^&*|+=[]{}`~?"
}
```


Sublime Packages:

Must-haves
* Emmet
* AutoFileName - Sublime Text plugin that autocompletes filenames
* Git - Plugin for some git integration into sublime text
* GitGutter - A Sublime Text 2/3 plugin to see git diff in gutter
* Sidebar Enhancements - Enhancements to Sublime Text sidebar. Files and folders.
* EditorConfig - Helps developers maintain consistent coding styles between different editors

Web/JS
* HTML-CSS-JS Prettify
* Jade: A comprehensive sublime text bundle for the Jade template language


ToCheckOut
* SublimeLinter - Interactive code linting framework for Sublime Text 3
* Alignment - Easy alignment of multiple selections and multi-line selections
* Bracket​Highlighter - Bracket and tag highlighter for Sublime Text
* EncodingHelper: Guess encoding of files, show in status bar, convert to UTF-8 from a variety of encodings
* FileDiffs: Shows diffs between the current file, or selection(s) in the current file, and clipboard, another file, or unsaved changes.
* Markdown​Editing


Experiment
* Theme - Spacegray
* Plain​Tasks
* SublimeCodeIntel - Full-featured code intelligence and smart autocomplete engine



## Sublime Text Settings
Check out [Preferences.sublime-settings](Preferences.sublime-settings)

## SublimeLinter Settings (Verify)
```
{
    "user": {
        "debug": false,
        "delay": 0.25,
        "error_color": "D02000",
        "gutter_theme": "none",
        "gutter_theme_excludes": [],
        "lint_mode": "background",
        "linters": {
            "pep8": {
                "@disable": false,
                "args": [],
                "disable": "",
                "enable": "",
                "excludes": [],
                "ignore": "",
                "max-line-length": null,
                "rcfile": "",
                "select": ""
            }
        },
        "mark_style": "outline",
        "no_column_highlights_line": false,
        "paths": {
            "linux": [],
            "osx": [
                "/usr/local/bin/"
            ],
            "windows": []
        },
        "python_paths": {
            "linux": [],
            "osx": [
                "/usr/local/bin/"
            ],
            "windows": []
        },
        "rc_search_limit": 3,
        "shell_timeout": 10,
        "show_errors_on_save": false,
        "show_marks_in_minimap": true,
        "syntax_map": {
            "html (django)": "html",
            "html (rails)": "html",
            "html 5": "html",
            "php": "html",
            "python django": "python"
        },
        "warning_color": "DDB700",
        "wrap_find": true
    }
}
```

## Tips and Tricks

#### Open Sublime Text from Terminal in macOS

**Step 1:** Before this make sure sublime is installed on your machine.

**Step 2:** Check ".bash_profile":


Now it's time to create your symbolic link in your PATH folder, BUT, before we do, let's check your profile file by using **sudo nano ~/.bash_profile**. These are the following lines that pertain to having subl work on the command line for Sublime Text:

open `sudo nano ~/.bash_profile` and add following exports into your bash_profile.

```
export PATH=/bin:/sbin:/usr/bin:/usr/local/sbin:/usr/local/bin:$PATH
export EDITOR='subl -w'
```
If you do make any edits to this file once you have closed it, you need to run the command:
```
source ~/.bash_profile 
```

**Step 3:** Create a symbolic link to Sublime Text:

Now in terminal just type following code to make link of Sublime to user/local/bin/subl

```
ln -s /Applications/Sublime\ Text.app/Contents/SharedSupport/bin/subl /usr/local/bin/subl
```

## Resource

* [Open Sublime Text from Terminal in macOS](https://stackoverflow.com/questions/16199581/open-sublime-text-from-terminal-in-macos)