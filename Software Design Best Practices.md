# Software Design Best Practices

* Follow the rules of [12 Factor Apps](https://12factor.net/).
* Logging optimized for centralization - parsable (JSON), searchable.
* Maintain separation of concerns.
* Declare and define all interfaces strictly, and test them well.
* Use automated test/build/deploy process using CI/CD.
* Declare test/build/release pipelines and infrastructure inside the code, and maintain it into version control.