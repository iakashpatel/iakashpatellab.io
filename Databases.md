# Databases

As Wikipedia puts it - "A database is an organized collection of data". There are numerous database software available catering to their own specific use cases. Broadly, most databases can be categorized in one of three distinct groups:

* **Key-Value Store** - Primitive databases either used as a building block to more complex databases, or for storing large blobs. e.g. LevelDB, Riak
* **Relational Database** - They specialize in dealing with structured data. They have advanced scripting capabilities using dialect of SQL. e.g. MySQL, PostgreSQL, Microsoft SQL Server, Oracle Database Server
* **Document Store** - These are more specialized databases in terms of features and guarantees they provide. They usually support a structured format like XML/JSON, but don't enforce a "structure" like Relational Databases do. They are sometimes referred to as NoSQL databases. e.g. MongoDB, CouchDB, RethinkDB

Choice of a database is extremely important - it can make or break a project/application. For most applications, Relational databases are an ideal choice.

## Relational Databases

Basic Stuff (that everyone must know):
* [Relational Model Theory](https://en.wikipedia.org/wiki/Relational_model)
* Normalization
* Available data types in PostgreSQL (or the target database)
* Working with `TABLE`s and `VIEW`s
* Constraints and Indexes
* CRUD (Create, Read, Update, Delete) operations using SQL
* Different kinds of Joins
* Aggregates (`GROUP BY`)

Intermediate Stuff (that you are expected to know before you can say "I know rdbms well"):
* Stored Functions/Procedures
* Triggers
* Transactions

Advanced Stuff (they can solve a lot of problems for you if you know them well):
* Optimizing Indexes

## Best Practices

[[ kb/db/best-practices | Read here ]]

## Resources

* [Beginner's Guide To SQL](http://www.sohamkamani.com/blog/2016/07/07/a-beginners-guide-to-sql/)
* [PostgreSQL Guide](http://postgresguide.com)