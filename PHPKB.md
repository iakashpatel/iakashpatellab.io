# PHP
PHP is the most popular server-side scripting language for creating dynamic web pages.
Creator: Rasmus Lerdorf
Founded: 1994

## Basic
If you want to be master in any laungauge the you need to make foundation strong. Here is some basic things about PHP that you should know. For more detail you can refer [[ http://php.net/manual/en/language.types.php | official doc ]]

### Booleans

When converting to boolean, the following values are considered FALSE:

the boolean FALSE itself
the integer 0 (zero)
the float 0.0 (zero)
the empty string, and the string "0"
an array with zero elements
the special type NULL (including unset variables)
SimpleXML objects created from empty tags
**Warning -1 is considered TRUE, like any other non-zero (whether negative or positive) number!**
```
var_dump((bool) "");        // bool(false)
var_dump((bool) 1);         // bool(true)
var_dump((bool) -2);        // bool(true)
var_dump((bool) "foo");     // bool(true)
var_dump((bool) 2.3e5);     // bool(true)
var_dump((bool) array(12)); // bool(true)
var_dump((bool) array());   // bool(false)
var_dump((bool) "false");   // bool(true)
```

### Interger

FALSE will yield 0 (zero), and TRUE will yield 1 (one).

**Warning Never cast an unknown fraction to integer, as this can sometimes lead to unexpected results.**

```
<?php
echo (int) ( (0.1+0.7) * 10 ); // echoes 7!
?>
```

#### Strings
Single quoted strings will display things almost completely "as is." Variables and most escape sequences will not be interpreted. The exception is that to display a literal single quote, you can escape it with a back slash \', and to display a back slash, you can escape it with another backslash \\ (So yes, even single quoted strings are parsed).

```
<?php
$total = 6;
echo 'Hello World \n Bye {$total}';
```
Double quote strings will display a host of escaped characters (including some regexes), and variables in the strings will be evaluated. An important point here is that you can use curly braces to isolate the name of the variable you want evaluated. For example let's say you have the variable $type and you what to echo "The $types are" That will look for the variable $types. To get around this use echo "The {$type}s are" You can put the left brace before or after the dollar sign. Take a look at string parsing to see how to use array variables and such.
```
<?php
$total = 6;
echo "Hello World \n Bye {$total}';
```

In detail you can read it [[ http://www.php.net/manual/en/language.types.string.php | here ]]

###Callable
In simple, a function that can be pass to another method or function to do some operation.

```
<?php
// cube is callable(Callback) function for array_map
function cube($n)
{
    return($n * $n * $n);
}

$a = array(1, 2, 3, 4, 5);
$b = array_map("cube", $a);
print_r($b);
?>
```
More detail [[ http://php.net/manual/en/language.types.callable.php | here ]]


### Workshop Time
Install workshop-manager
```
curl https://php-school.github.io/workshop-manager/workshop-manager.phar
mv workshop-manager.phar /usr/local/bin/workshop-manager
chmod +x /usr/local/bin/workshop-manager
workshop-manager verify
```
Install learnyouphp

```
workshop-manager install learnyouphp
```

Now you are familiar with PHP. Here are few more topics that you should know in PHP.


  - Control Structures (if..else, while, do ...while, for, foreach, switch, goto )
  - Functions (User-defined functions, Function arguments, Returning values, Variable functions, Scalar type declarations - PHP 7, Return type declarations - PHP 7)
  - Classes and Objects (Constructors and Destructors, visibility of a property - public/private/protected, Inheritance, Scope Resolution Operator (::) , Class Abstraction, Object Interfaces, Anonymous classes - PHP 7, Last but not least - Traits)
  - Magic methods
  - Namespaces
  - Exceptions





## PHP 7
During 2014 and 2015, a new major PHP version was developed, which was numbered PHP 7

  - Scalar type declarations
  - Return type declarations
  - Null coalescing operator
  - Spaceship operator
  - Constant arrays using define()
  - Anonymous classes
  - Closure::call()
  - Integer division with intdiv()

You can find detail about above topic at [[ http://php.net/manual/en/migration70.new-features.php#migration70.new-features.spaceship-op | PHP 7 features ]]

### Workshop Time
Install php7way - A workshop related to PHP 7 features
```
workshop-manager install php7way
```


# Laravel

Laravel is a free, open-source PHP web framework, created by **Taylor Otwell** in June, 2011 and intended for the development of web applications following the model–view–controller (MVC) architectural pattern and based on Symfony.

### Installation

```
> composer global require laravel/installer
> laravel new blog
```

### Dev server

```
> php artisan serve
```

## Valet

Do you hate localhost:port when you test locally? Are you tired to do `php artisan serve` after every change? Then you should try valet. Valet is handy tool to manage nginx configuration for your laravel app.
As per doc :

> Valet provides a blazing fast local development environment with minimal resource consumption, so it's great for developers who only require PHP / MySQL and do not need a fully virtualized development environment.

### Installation

Valet requires macOS and Homebrew. Before installation, you should make sure that no other programs such as Apache or Nginx are binding to your local machine's port 80.


  -     Install or update Homebrew to the latest version using brew update.
  -     Install PHP 7.2 using Homebrew via brew install php@7.2.
  -     Install Composer.
  -     Install Valet with Composer via composer global require laravel/valet. Make sure the ~/.composer/vendor/bin directory is in your system's "PATH".
  -     Run the valet install command. This will configure and install Valet and DnsMasq, and register Valet's daemon to launch when your system starts.

Once Valet is installed, try pinging any *.test domain on your terminal using a command such as ping foobar.test. If Valet is installed correctly you should see this domain responding on 127.0.0.1.

Valet will automatically start its daemon each time your machine boots. There is no need to run valet start or valet install ever again once the initial Valet installation is complete.

### Serving sites

Once Valet is installed, you're ready to start serving sites. Valet provides two commands to help you serve your Laravel sites: **park and link.**

**The park Command**

    Create a new directory on your Mac by running something like **mkdir ~/Sites**. Next,** cd ~/Sites** and run **valet park**. This command will register your current working directory as a path that Valet should search for sites.
    Next, create a new Laravel site within this directory: **laravel new blog**.
    Open **http://blog.test** in your browser.

That's all there is to it. Now, any Laravel project you create within your "parked" directory will automatically be served using the http://folder-name.test convention.

**The link Command**

The link command may also be used to serve your Laravel sites. This command is useful **if you want to serve a single site in a directory and not the entire directory.**

    To use the command, navigate to one of your projects and run valet link app-name in your terminal. Valet will create a symbolic link in **~/.config/valet/Sites** which points to your current working directory.
    After running the link command, you can access the site in your browser at **http://app-name.test.**

To see a listing of all of your linked directories, run the valet links command. You may use valet unlink app-name to destroy the symbolic link.

You can explore more about valet [[ https://laravel.com/docs/5.7/valet | here ]]

## Developer Tools


  - [[ https://laravel-news.com/laravel-tinker | Tinker ]]
  - [[ https://github.com/barryvdh/laravel-debugbar | Laravel Debugbar ]]


## Laravel Basics



  - [[ https://laravel.com/docs/5.7/lifecycle | Request Lifecycle ]]
  - [[ https://laravel.com/docs/5.7/structure | Directory Structure ]]
  - [[ https://laravel.com/docs/5.7/providers | Providers ]]
  - [[ https://laravel.com/docs/5.7/routing | Routing ]]
  - [[ https://laravel.com/docs/5.7/middleware | Middleware ]]
  - [[ https://laravel.com/docs/5.7/controllers | Controllers  ]]
  - [[ https://laravel.com/docs/5.7/requests | Requests ]]
  - [[ https://laravel.com/docs/5.7/responses | Response ]]
  - [[ https://laravel.com/docs/5.7/session | Session ]]
  - [[ https://laravel.com/docs/5.7/validation | Validation ]]
  - [[ https://laravel.com/docs/5.7/errors | Error Handling ]]
  - [[ https://laravel.com/docs/5.7/logging | Logging ]]
  - [[ https://laravel.com/docs/5.7/helpers | Helpers ]]



## Intermediate


 - [[ https://laravel.com/docs/5.7/authentication | Authentication ]]
 - [[ https://laravel.com/docs/5.7/authorization | Authorization  ]]
 - [[ https://laravel.com/docs/5.7/artisan | Artisan Console ]]
 - [[ https://laravel.com/docs/5.7/collections | Collections ]]
 - [[ https://laravel.com/docs/5.7/events | Events ]]
 - [[ https://laravel.com/docs/5.7/queues | Queues ]]
 - [[ https://laravel.com/docs/5.7/database | Database]] - [[ https://laravel.com/docs/5.7/eloquent | Eloquent  ORM ]] + Query builder



## Adavance



 - [[ https://laravel.com/docs/5.7/container | Service Container ]]
 - [[ https://laravel.com/docs/5.7/facades | Facades ]]
 - [[ https://laravel.com/docs/5.7/broadcasting | Broadcasting ]]
 - [[ https://laravel.com/docs/5.7/scheduling | Task Scheduling ]]

## Testing

- [[ https://laravel.com/docs/5.7/http-tests | HTTP Tests ]]
- [[ https://laravel.com/docs/5.7/console-tests | Console Tests ]]
- [[ https://laravel.com/docs/5.7/mocking | Mocks ]]
- [[ https://laravel.com/docs/5.7/database-testing | Factories ]]



## Video Tutorial

[[ https://laracasts.com/series/laravel-from-scratch-2018/episodes/1 | Laravel 5.7 ]]

## Want to learn more?

[[ https://github.com/chiraggude/awesome-laravel | Awesome Laravel ]]
