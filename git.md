# git

Git is central part of development workflow for all kinds of software. You will find that it's a very powerful utility, and it will make it worthwhile if you learn and master it.

## Setup

[[ kb/git/setup | Set up ]] git on your new user/machine.

Basic Stuff (that everyone must know):

* How git works (Hint: it stores changes since last commit)
* Initialize/create/clone a git repository
* Check working directory status and interpret what it means
* Add, remove, stage, unstage files
* Commit the staged changes
* Viewing history and being able to figure out what
* Pull and push changes from remote server
* Create, merge, remove, and push branches/tags, and switch between branches
* Dealing with uncommitted stuff with stash

Intermediate Stuff (that you are expected to know before you can say "I know git well"):

* Know the differences between rebasing and merging
* Resolving conflicts
* How to undo commits from a branch (revert, hard reset, rebase)
* How to move commits around from one branch to other (cherry-pick)
* Knowing how remotes work and being able to manually add/edit/delete remotes

Advanced Stuff (that can save you a lot of time and effort):

* Narrowing down to a bug with `git bisect` and `git blame`
* Rewriting history (with `rebase`)
* Finding "lost" commits with `git reflog`

Miscellaneous Stuff (that is not relevant for workflows we use, but good to know)

* Working with git submodules

## Resources

* [Git-it Cmd](https://github.com/jlord/git-it/blob/master/original-readme.md)
* [Git-it Desktop](https://github.com/jlord/git-it-electron) (Hands-on git workshop to learn basics of git and github)
* [Try Git](https://try.github.io) (This is where most should start)
* [Git Immersion](http://gitimmersion.com/index.html) (Interactive guide for learning basics)
* [Git Book](https://git-scm.com/book) (Probably contains everything about everything git)
* [Git Ready](http://gitready.com/) (Good source to learn advanced concepts)
* [Git Internals](https://github.com/pluralsight/git-internals-pdf) (A great small book about how git handles things internally)
* [How to undo (almost) anything with Git](https://github.com/blog/2019-how-to-undo-almost-anything-with-git)
* [Learn Git Branching](http://learngitbranching.js.org/)
* [Easier Rebases With Git](http://neversaw.us/2014/01/19/easier-rebases-with-git/)
* [Code Archaeology With Git](http://jfire.io/blog/2012/03/07/code-archaeology-with-git/)

## Extras

[git-extras](https://github.com/tj/git-extras) is a great tool that adds a lot of nice-to-have shortcuts as additional git commands. This is strictly optional though.