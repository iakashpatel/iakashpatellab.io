# Posix Command Line

Terminal is a place where all developers will spend quite a bit of time. It's crucial that everyone becomes familiar with various command line tools/utilities. Below is the list of a few activities that you should know how to do from command line:

* Navigating and altering the filesystem
  * Moving around the filesystem
  * Viewing files including searching through them to find information you need
  * Creating, deleting, and altering files/directories
  * How unix filesystem permission model works
  * Changing ownership of files and directories
* Working with processes
  * Listing processes and searching for specific process
  * Monitoring and killing processes
* Knowing what stdin/out/err is and working with it
* Being able to use unix pipes

We believe our competency as software developers lies in being able to do things quickly. We have a recommended terminal setup for everyone to get them started quickly. If you see something you don't like or you think you can improve something, feel free to customize your dotfiles and design your own command line experience. The default setup consists of ZSH and oh-my-zsh.

In addition to this, creating a new pair of SSH keys using `ed25519` is recommended. You will be adding them to your git server for authentication.

Here's a list of additional command line utilities that you will find helpful:

* brew
* pbcopy/pbpaste
* http (Httpie)
* tmux