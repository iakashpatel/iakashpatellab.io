# JavaScript and Node.js

Prerequisites:

* [[ kb/javascript | JavaScript ]]

Basic:
* Node.js Fundamentals and Core APIs
  * [LearnYouNode](https://github.com/workshopper/learnyounode)
  * [API](https://nodejs.org/api)
* HTTP Servers
  * HTTP Fundamentals
  * [Intro To REST](https://www.youtube.com/watch?v=YCcAE2SCQ6k)
  * How HTTP Servers work, how Node.js HTTP Server deals with concurrency and how is it different from other languages
* Building blocks of Webapps
  * [Express](https://github.com/azat-co/expressworks)
  * [MongoDB](https://github.com/evanlucas/learnyoumongo)
* Working with modules
* Automated Testing (https://github.com/finnp/test-anything)


## Resources

* [Node For Beginners](https://github.com/rockbot/node-for-beginners)
* [Awesome Node](https://github.com/sindresorhus/awesome-nodejs#resources)