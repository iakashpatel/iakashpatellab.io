# Configuration

## System Preferences

* Dock -> Adjust orientation, size and magnification. Keeping Dock on the left is efficient for many. Also optionally hide it.
* Mission Control -> Configure Hot Corners. Quick access to desktop and even Dashboard can be helpful.
* Security and Privacy -> Allow apps downloaded from *Everywhere*, require password after screensaver is on, disable autologin.
* Spotlight -> Remove Fonts, Documents, Webpages, Images and Movies from search results
* Display -> Turn-off the display mirroring icon in menu bar
* Energe Saver -> Set the sleep and put your hard disk to sleep depending on the sleep support on your machine.
* Mouse -> Turn off Scroll direction: natural
* Sound -> Turn on Show volume in menu bar option. Helps to identify easily whether audio driver work.
* Sharing -> Turn on Screen Sharing, File Sharing and Remote Login. Allow All users to share screen. Also enable "Anyone may request permission to control the screen" from "Computer Settings"
* App Store -> Disable Automatic updates
* User & Groups -> Setup proper account name, password and picture

## Finder Preferences

* General -> Hide Hard Disks and Connected Servers from Desktop and set new Finder window to show user's home
* Sidebar -> Remove Tags and Devices so they're not a hinderance. Show anything else that you feel would be helpful. The aim here is to minimize the list, not have everything turned on. Adding home directory and workspace helps.

## Software

* Homebrew and Homebrew Cask
* XCode

```
xcode-select --install
```

* NVM

```
wget -qO- https://raw.githubusercontent.com/creationix/nvm/v0.25.4/install.sh | bash
```

## Homebrew Apps

* git
* git-extras
* git-flow

## Homebrew Cask Apps

* qlstephen - Preview plain text files without a file extension. Example: README, CHANGELOG, etc.
* qlmarkdown - Preview Markdown files
* quicklook-json - Preview JSON files
* quicklook-csv - Preview CSV files
* webpquicklook - Preview WebP images
* macdown - Markdown Editor
* LICEcap - Gif screen capture utility

## Terminal

* Install and configure oh-my-zsh (details later)
* Add env.sh that configures the environment and add it to .zshrc
* Add homebrew path, aliases etc to env.sh
* Create SSH Keys if you don't already have it

```
ssh-keygen -t rsa -C "your_email@example.com"
```

* Configure git

```
git config --global user.name "Your Name Here"
git config --global user.email "your_email@youremail.com"
```

* Set Sublime text as default mergetool

```
git config --global mergetool.sublime.cmd "subl -w \$MERGED"
git config --global mergetool.sublime.trustExitCode false
git config --global merge.tool sublime
git mergetool -y
```

* Create global gitignore and put the usual stuff in there
* Create subline shortcut for command line

```
ln -s /Applications/Sublime\ Text.app/Contents/SharedSupport/bin/subl /usr/local/bin/subl
```

## Misc

### NTFS Support

* Install OSXFuse

```
brew cask install osxfuse
```

* Install ntfs-3g
```
brew install ntfs-3g
```

* Replace `mount_ntfs` script in /sbin (Might have to temporarily disable System Integrity Protection in OSX 10.11+)
```
sudo mv "/Volumes/MacHD/sbin/mount_ntfs" "/Volumes/MacHD/sbin/mount_ntfs.orig"
sudo ln -s /usr/local/sbin/mount_ntfs "/Volumes/MacHD/sbin/mount_ntfs"
```

## Apps to Copy Over

* Dropbox
* Firefox
* Flux
* GitX
* Google Chrome
* Medis
* MongoHub
* Nylas N1
* Slack
* Sublime Text
* The Unarchiver
* VirtualBox

## Apps to evaluate

* Alfred
* iTerm 2
* Valentina Studio - MySQL, PostgreSQL GUI
* Spectacle: Don't waste time resizing and moving your windows. Spectacle makes this very easy and is open source.
