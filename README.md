---
description: Python Knowledge Base - 5 November 2018
---

### Python

---


Basic:

* [Python Syntax](https://www.tutorialspoint.com/python/python_basic_syntax.htm)
* [Data types (Important: List and Directories )](https://docs.python.org/3/library/datatypes.html)
* [string manipulation](https://programminghistorian.org/en/lessons/manipulating-strings-in-python)
* [Operators (Important: 'in' Operator and Bitwise Operator)](https://data-flair.training/blogs/python-operator/)
* [Conditional Statements and Control Flow](https://realpython.com/python-conditional-statements/)
* [Functions](https://www.tutorialspoint.com/python/python_functions.htm)
* [Loops](https://www.learnpython.org/en/Loops)


Advance:

* [Iterators of Directories](https://dev-notes.eu/2017/09/iterating-over-dictionary-in-python/)
* [Lambda Syntax and Expressions](https://www.programiz.com/python-programming/anonymous-function)
* [Regular Expressions](https://docs.python.org/3/howto/regex.html)
* [Exception handling with Try and Catch](https://www.programiz.com/python-programming/exception-handling)
* [Object-Oriented Programming](https://www.programiz.com/python-programming/object-oriented-programming)
* [Files I/O Functions and Utilities](https://www.tutorialspoint.com/python/python_files_io.htm)


Complete Python Tutorials (Basic + Advance)

- [Learn Python on codecademy](https://www.codecademy.com/learn/learn-python)
- [Python Tutorials - Corey Schafer](https://www.youtube.com/playlist?list=PL-osiE80TeTt2d9bfVyTiXJA-UTHn6WwU)


Frameworks:

- Flask (Web)
    - [Official Flask Documentations](http://flask.pocoo.org/docs/1.0/)
    - [Flask Learning Checklist](https://www.fullstackpython.com/flask.html)
    - [Python Flask Tutorial Playlist](https://www.youtube.com/watch?v=MwZwr5Tvyxo&list=PL-osiE80TeTs4UjLw5MM6OjgkjFeUxCYH)
- Django (Web)
    - [Official Documentations](https://docs.djangoproject.com/en/2.1/)
    - [Django Learning Checklist](https://www.fullstackpython.com/django.html)
- Scrapy (Web Scrapping)
    - [Official Documentations](https://docs.scrapy.org/en/latest/)
    - [Video Series On ScrappingHub](https://learn.scrapinghub.com/scrapy/)
    - [Learn Scrapy on Tutorials Point](https://www.tutorialspoint.com/scrapy/)


Official Coding Style Guide:

- https://www.python.org/dev/peps/pep-0008/ (Must Follow for Standard Practises)


Resources:

- [Official Python 3.x Tutorial](https://docs.python.org/3/tutorial/index.html)
- [Full Stack Python - Table of Contents](https://www.fullstackpython.com/table-of-contents.html)
- [Python - Tutorials Point](https://www.tutorialspoint.com/python/)
