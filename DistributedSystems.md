# Distributed Systems

A distributed system is a piece of software that ensures that a collection of independent computers appears to its users as a single coherent system.

* Microservices
* Message Queues and RPC
* [[ kb/distributed-systems/containers | Containers ]]
* Horizontal vs vertical Scaling
* Load Balancers
* Scaling Databases
* Service Discovery




# Containers and Docker

Docker is two things - a container image format, and a container runtime. Docker generates immutable container images that can be distributed and run on multiple machines - making all the environment identical. Docker makes creating and managing distributed systems a lot easier.

It is imperative to know about fundamental infrastructure paradigms before moving on to learn more about Docker. Here's a list:

* Containers, and their differences from Virtual Machines
* Linux FileSystem, Processes and Permissions
* [Distributed Systems](Readme.md)

Bare Minimum Stuff (that everyone must know):
* Pulling and Running Docker images using `docker` command-line tool.
* Using `docker-compose` utility to run services.
* Creating, reading and editing `docker-compose.yml` files.
* Starting, stopping and restarting containers.
* Printing/Listening to logs from an individual container.

Intermediate Stuff (that you should know before writing any distributed/horizontally scalable backend):
* Write a `Dockerfile` from scratch that builds an image for your service.
* Describe the whole service using `docker-compose.yml`.
* Inspect and debug running/stopped containers, and execute commands inside a running container.
* Docker networking - none, host, bridged networks
* Docker Volumes - mounting host directories inside the container, volumes from other containers, and Docker Volumes

Advanced:
* Working with different logging drivers
* Working with orchestration systems like Kubernetes, Mesos or Amazon ECS
* Working with other container runtimes - rkt, runC

## Resources
- [Docker Curriculum By Prakhar Shrivastav](https://prakhar.me/docker-curriculum)