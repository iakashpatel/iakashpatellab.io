# JavaScript

JavaScript is one of the core components of all the work we do here. It's a language that practically runs on any platform. May it be browsers, servers, desktops, mobile devices or even embedded devices. No matter what you're building here, you will have to deal with JavaScript in one way or the other. It's going to be one of those tools that can take you places if you've mastered it.

Bare Minimum Stuff (that everyone should know within their first few days):

* Language Constructs (syntax, patterns etc.)
  * Variables and Scope (var, let, const, different data types JS, closures, arrow functions)
    * [Scope Chains And Closures](https://github.com/jesstelford/scope-chains-closures)
    * [What is this](https://howtonode.org/what-is-this)
  * Basic data structures supported by JS (strings, arrays, objects)
* Sync v/s Async
  * Single threaded nature of JS and how it's different from other languages in handling concurrency
  * Various async patterns (callbacks, Promises, eventemitters, streams)
    * [Promise Workshop](https://github.com/stevekane/promise-it-wont-hurt)
* Functional Programming Concepts
  * How to think in functional programming mindset
  * Function as an object constructs (e.g. bind, call, apply)
  * Map, reduce, filter etc. helpers
    * [Functional JavaScript Workshop](https://github.com/timoxley/functional-javascript-workshop)

Intermediate Stuff (that you should know if you call yourself a JS programmer):

* [Prototypal Inheritance in JS](https://github.com/sporto/planetproto)
* Event Loop and how it works
* Newer language constructs (ES2016 and beyond)
  * New data structures supported by JS (Set, Map, WeakSet, WeakMap)
  * for .. of loops, iterators and generators
  * async/await

Advanced:

* Advanced functional programming concepts (e.g. currying, immutable data structures)
  * [Currying](https://github.com/kishorsharma/currying-workshopper)
  * [Immutable Data Structures in JS](http://jlongster.com/Using-Immutable-Data-Structures-in-JavaScript)
  * [Intro to Immutable.js](https://auth0.com/blog/intro-to-immutable-js/)

## Resources:

* [Eloquent JavaScript: The Book](http://eloquentjavascript.net/)
* [Mozilla JavaScript Guide/Reference](https://developer.mozilla.org/en-US/docs/Web/JavaScript)

### Classes in JavaScript with ES6

* [Classes in JavaScript with ES6](https://www.youtube.com/watch?v=T-HGdc8L-7w)    

### ES6 in one video

* [ES6 Workshop](https://www.youtube.com/watch?v=trvnP7EsAHA)   
* [ES6 JavaScript - The Basics](https://www.youtube.com/watch?v=IEf1KAcK6A8)     


### Series

* [JavaScript ES6](https://www.youtube.com/watch?v=HIS8juawTmM&list=PLVHlCYNvnqYouIVj3IgK3RmzpnWMaoqkw)   
* [Let's Learn ES6](https://www.youtube.com/watch?v=LTbnmiXWs2k&list=PL57atfCFqj2h5fpdZD-doGEIs0NZxeJTX)    

### Articles

* [ECMAScript 6 Tutorial](http://ccoenraets.github.io/es6-tutorial/)   
* [ES6 Workshop](http://slides.com/kentcdodds/es6-workshop#/)   
* [ES6 Tutorial](https://www.tutorialspoint.com/es6/index.htm)    
* [Arrow Functions](https://zellwk.com/blog/es6/#arrow-functions)   
* [ES6 In Depth](https://hacks.mozilla.org/category/es6-in-depth/)    
* [8 Awesome ES6 Features](https://blog.jscrambler.com/8-awesome-es6-features/)    
* [JavaScript - mdn](https://developer.mozilla.org/bm/docs/Web/JavaScript)   
* [ES6 for beginners](https://codeburst.io/es6-tutorial-for-beginners-5f3c4e7960be)   
* [ECMAScript 6 Complete Tutorial](http://qnimate.com/post-series/ecmascript-6-complete-tutorial/)   
* [You Don't Know JS: ES6 & Beyond](https://github.com/getify/You-Dont-Know-JS/tree/master/es6%20%26%20beyond)    
* [Everything I learned from ES6 for Everyone](https://medium.com/craft-academy/everything-i-learned-from-es6-for-everyone-ff93ebc64b86)    